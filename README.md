#Namepair

#简介
想了解你与你心仪的他（她）的姓名缘分配对的秘密吗？请输入您与他（她）的中文姓名立即开始姓名配对！

#演示
http://www.wandoujia.com/apps/com.mj.namepair

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")